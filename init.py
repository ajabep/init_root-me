#!/usr/bin/env python3
import argparse
import json
import os
import os.path
import shutil
from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader
import requests

class StupidPeopleException(Exception):
    pass

try:
    from urllib.parse import urlparse
except:
    raise StupidPeopleException("NTM avec ton python2 frer")


CONFIGFILE = os.path.expanduser('~/.config/init-root-me.json')
TEMPLATE_DIRECTORY = '.template'
ID_CATEGORY_EXPLOIT = 203
SESSION = None


def __print_xfixed(prefix, suffix, *args, **kwargs):
    args = list(args)
    args.insert(0, prefix)
    args.append(suffix)
    args = tuple(args)
    print(*args, **kwargs)

def error(*args, **kwargs):
    __print_xfixed("\033[31;1m", "\033[0m", *args, **kwargs)

def success(*args, **kwargs):
    __print_xfixed("\033[32;1m", "\033[0m", *args, **kwargs)

def warning(*args, **kwargs):
    __print_xfixed("\033[33;1m", "\033[0m", *args, **kwargs)

def info(*args, **kwargs):
    __print_xfixed("\033[34;1m", "\033[0m", *args, **kwargs)

def debug(*args, **kwargs):
    __print_xfixed("\033[38;5;8;1m", "\033[0m", *args, **kwargs)


ext = {
    'c': 'c'
}

def tablesoup_to_dict(soup):
    data = {}
    table_body = soup.find('tbody')
    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        if len(cols) == 0:
            data[cols[0]] = []
        elif len(cols) == 2:
            data[cols[0]] = cols[1]
        else:
            data[cols[0]] = cols[1:]
    return data

def get_chall_full_details(chall):
    r = requests.get('https://www.root-me.org/{}'.format(chall['url_challenge']),
                     cookies={'spip_session': SESSION})

    soup = BeautifulSoup(r.text, 'html.parser')

    sources_codes = soup.select('.crayon.challenge-descriptif-{} .coloration_code .cadre'
                                .format(chall['id_challenge']))
    chall['sources_codes'] = []
    if len(sources_codes) > 0:
        for source in sources_codes:
            chall['sources_codes'].append(
                {
                    'lang': list(source.children)[0]['class'][0],
                    'code': source.get_text()
                }
            )

    access_table = soup.select('.txs.mauto')
    chall['access'] = {}
    if len(access_table) > 1:
        error("Don't know how to manage several access")
    elif len(access_table) == 1:
        access_table = tablesoup_to_dict(access_table[0])
        if access_table['Protocole'] == 'TCP':
            chall['remote'] = {
                'proto': 'TCP',
                'host': access_table['Hôte'],
                'port': access_table['Port'],
            }
        if 'Accès SSH' in access_table.keys():
            chall['access']['ssh_host'] = access_table['Accès SSH'].split('\xa0')[0].split('@')[1].split(' ')[0]
            chall['access']['ssh_port'] = access_table['Accès SSH'].split('\xa0')[0].split(' -p ')[1].split(' ')[0]
            chall['access']['ssh_user'] = access_table['Accès SSH'].split('\xa0')[0].split('@')[0].split(' ')[-1]
            chall['access']['ssh_passwd'] = access_table['Mot de passe']

    return chall


def treat_challs(challs):
    for chall in challs:
        path = lambda filename: os.path.join(chall['titre'], filename)
        formats = get_chall_full_details(chall)

        try:
            shutil.copytree(TEMPLATE_DIRECTORY, chall['titre'], symlinks=True)
        except FileExistsError:
            error("The challenge '{}' already exists".format(chall['titre']))
            continue
        env = Environment(loader=FileSystemLoader(TEMPLATE_DIRECTORY))

        def render_file(filename):
            with open(path(filename), 'w') as dst:
                dst.write(
                    env.get_template(filename).render(**formats)
                )
        render_file('README')

        if 'ssh_cmd' in formats['access'].keys():
            render_file('conn')
        else:
            os.remove(path('conn'))

        if 'sources_codes' in formats.keys():
            for k, source in enumerate(formats['sources_codes']):
                if len(formats['sources_codes']) == 1:
                    k = 'chall'
                with open(path("{}.{}".format(k, ext[source['lang']])), 'w') as src:
                    src.write(source['code'])

        with open(path('chall.json'), 'w+') as details:
            details.write(json.dumps(chall))


def is_url(x):
    result = urlparse(x)
    for x in [result.scheme, result.netloc, result.path]:
        if x in ['', None]:
            raise argparse.ArgumentTypeError('Invalid URL')
    return x


def login(username, passwd):
    r = requests.post('https://api.www.root-me.org/login', data={
        'login': username,
        'password': passwd
    })
    if "info" not in r.json()[0].keys():
        raise Exception("Bad username or password")
    return r.json()[0]['info']['spip_session']


def get_challenges(**kwargs):
    challs = []
    debut_challenges = 0
    must_continue = True
    while must_continue:
        kwargs['debut_challenges'] = debut_challenges

        r = requests.get('https://api.www.root-me.org/challenges',
                         cookies={'spip_session': SESSION}, params=kwargs)
        resp = r.json()
        challs.extend(resp[0].values())
        debut_challenges += 50

        must_continue = False
        for obj in resp:
            if isinstance(obj, dict) and 'rel' in obj.keys() and obj["rel"] == "next":
                must_continue = True
    return challs


def get_chall_per_id(id_challenge):
    r = requests.get('https://api.www.root-me.org/challenges/{}'.format(id_challenge),
                     cookies={'spip_session': SESSION})
    resp = r.json()
    if not isinstance(resp, dict):
        raise ValueError("Challenge {} is private".format(id_challenge))
    return resp


def get_challs_per_url(urls):
    challs_wanted = []
    urls_path = [urlparse(url).path[1:] for url in urls]
    challs = get_challenges(id_rubrique=ID_CATEGORY_EXPLOIT)

    for chall in challs:
        try:
            chall_details = get_chall_per_id(chall['id_challenge'])
        except ValueError:
            continue
        if chall_details['url_challenge'] in urls_path:
            chall_details['id_challenge'] = chall['id_challenge']
            challs_wanted.append(chall_details)

    return challs_wanted


def get_creds():
    if not os.path.isfile(CONFIGFILE):
        warning("Configuration file does not exists.")
        info("We will create it :D")
        error("IT WILL WRITE YOUR CREDENTIALS IN CLEAR TEXT ON YOUR DISK." + "\n"
              "CONCIDER THIS VULNERABILITY BEFORE CONTINUING.")
        username = input("Login: ")
        passwd = input("Password: ")
        with open(CONFIGFILE, 'w+') as dump:
            dump.write(json.dumps({
                'login': username,
                'passwd': passwd,
            }))
        error("YOUR CREDENTIALS HAS BEEN WRITEN IN CLEAR TEXT IN THE FILE {}.".format(CONFIGFILE))

    with open(CONFIGFILE, 'r') as dump:
        c = json.loads(dump.read())
        username = c['login']
        passwd = c['passwd']

    return username, passwd


def main():
    global SESSION
    parser = argparse.ArgumentParser(description='Init a directory with everything to start an ' +
                                     '**EXPLOITATION** Root-Me challenge with confidence :)')
    parser.add_argument('url', type=is_url, nargs='+')

    args = parser.parse_args()
    username, passwd = get_creds()

    info("Logging in")
    SESSION = login(username, passwd)
    info("Search challenges and get details")
    challs = get_challs_per_url(args.url)

    info("Creating files")
    # Put this business flow in another function, to improve the ability to fork it
    treat_challs(challs)


if __name__ == '__main__':
    main()
